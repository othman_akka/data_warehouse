create or replace type region_type as object(
id_region number(20),
region_name varchar2(200),

MEMBER function id_region_dw return  number,
MEMBER function region_name_dw return varchar2
)
/

create or replace type body region_type as
    MEMBER function  id_region_dw return  number is
    BEGIN
        return id_region;
    end;

    MEMBER function region_name_dw return varchar2 is
    BEGIN
        return region_name;
    end;
end;
/