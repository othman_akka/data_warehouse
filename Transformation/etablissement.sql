create or replace type etablissement_type as object(
id_etablissement number(20),
id_ville number(20),
nombre_public varchar2(200),
nombre_prive varchar2(200),
ville varchar2(200),
id_region number(20),
date_e varchar2(200),	

MEMBER function id_etablissement_dw return  number,
MEMBER function id_ville_dw return number,
MEMBER function nombre_public_dw return  varchar2,
MEMBER function nombre_prive_dw return  varchar2,
MEMBER function ville_dw return varchar2,
MEMBER function id_region_dw return  number,
MEMBER function date_e1_dw  return  number,
MEMBER function date_e2_dw  return  number
)
/

create or replace type body etablissement_type as
    MEMBER function  id_etablissement_dw return  number is
    BEGIN
        return id_etablissement ;
    end;

    MEMBER function id_ville_dw return number is
    BEGIN
        return id_ville;
    end;

    MEMBER function nombre_public_dw return varchar2 is
    BEGIN
        return nombre_public;
    end;

    MEMBER function nombre_prive_dw return varchar2 is
    BEGIN
        return nombre_prive;
    end;

    MEMBER function ville_dw return varchar2 is
    BEGIN
        return ville;
    end;

    MEMBER function id_region_dw return number is
    BEGIN
        return id_region;
    end;
	   
	MEMBER function date_e1_dw return number is
    BEGIN
        return substr(date_e,1,4);
    end;
	    
	MEMBER function date_e2_dw return number is
    BEGIN
        return substr(date_e,6,4);
    end;
end;
/