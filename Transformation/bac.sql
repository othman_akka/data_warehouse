create or replace type bac_type as object(
id_serie number(20),
serie_name varchar2(200),

MEMBER function id_serie_dw return  number,
MEMBER function serie_name_dw return varchar2
)
/

create or replace type body bac_type as
    MEMBER function id_serie_dw return  number is
    BEGIN
        return id_serie;
    end;

    MEMBER function serie_name_dw return varchar2 is
    BEGIN
        return serie_name;
    end;
end;
/