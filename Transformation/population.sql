create or replace type population_type as object(
id_population number(20),
nombre_demande number(20),
nombre_accepte number(20),
id_classe number(20),
id_serie number(20),	
date_p varchar2(200),

MEMBER function id_population_dw return  number,
MEMBER function nombre_demande_dw return number,
MEMBER function nombre_accepte_dw return  number,
MEMBER function id_classe_dw return  number,
MEMBER function id_serie_dw return number,
MEMBER function date_p1_dw return  number,
MEMBER function date_p2_dw return  number
)
/

create or replace type body population_type as
    MEMBER function  id_population_dw return  number is
    BEGIN
        return id_population;
    end;

    MEMBER function nombre_demande_dw return number is
    BEGIN
        return nombre_demande;
    end;

    MEMBER function nombre_accepte_dw return number is
    BEGIN
        return nombre_accepte;
    end;

    MEMBER function id_classe_dw return number is
    BEGIN
        return id_classe;
    end;

    MEMBER function id_serie_dw return number is
    BEGIN
        return id_serie;
    end;

    MEMBER function date_p1_dw return number is
    BEGIN
        return substr(date_p,1,4);
    end;
	    
	MEMBER function date_p2_dw return number is
    BEGIN
        return substr(date_p,6,4);
    end;
end;
/