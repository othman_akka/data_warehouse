create or replace type classe_type as object(
id_classe number(20),
classe_name varchar2(200),

MEMBER function id_classe_dw return  number,
MEMBER function classe_name_dw return varchar2
)
/

create or replace type body classe_type as
    MEMBER function id_classe_dw return  number is
    BEGIN
        return id_classe;
    end;

    MEMBER function classe_name_dw return varchar2 is
    BEGIN
        return classe_name;
    end;
end;
/