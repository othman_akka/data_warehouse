create or replace type mention_inscrit_type as object(
id_inscrit number(20),
id_classe number(20),
id_mention number(20),
nombre number(20),
date_m varchar2(200),	

MEMBER function id_inscrit_dw return  number,
MEMBER function id_classe_dw return number,
MEMBER function id_mention_dw return  number,
MEMBER function nombre_dw return  number,
MEMBER function date_m1_dw return number,
MEMBER function date_m2_dw return  number
)
/

create or replace type body mention_inscrit_type as
    MEMBER function  id_inscrit_dw return  number is
    BEGIN
        return id_inscrit;
    end;

    MEMBER function id_classe_dw return number is
    BEGIN
        return id_classe;
    end;

    MEMBER function id_mention_dw return number is
    BEGIN
        return id_mention;
    end;
	
	MEMBER function nombre_dw return number is
    BEGIN
        return nombre;
    end;

    MEMBER function date_m1_dw return number is
    BEGIN
        return substr(date_m,1,4);
    end;

    MEMBER function date_m2_dw return number is
    BEGIN
        return substr(date_m,6,4);
    end;
end;
/