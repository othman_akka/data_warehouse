create or replace type mention_type as object(
id_mention number(20),
mention_name varchar2(200),	

MEMBER function id_mention_dw return  number,
MEMBER function mention_name_dw return varchar2
)
/

create or replace type body mention_type as
    MEMBER function  id_mention_dw return  number is
    BEGIN
        return id_mention ;
    end;

    MEMBER function mention_name_dw return varchar2 is
    BEGIN
        return mention_name;
    end;

end;
/




















