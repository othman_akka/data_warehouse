CREATE TABLE cpge(
id_fait number(20) not null ,
id_pob number(20),
id_eta number(20),
id_date number(20),
id_mention number(20),
nombre_demande number(20),
nombre_accepte number(20),
nombre_mention number(20),
PRIMARY KEY (id_fait),
FOREIGN KEY (id_pob) REFERENCES dim_population(id_pob),
FOREIGN KEY (id_eta) REFERENCES dim_etablissement(id_eta),
FOREIGN KEY (id_date) REFERENCES dim_date(id_date),
FOREIGN KEY (id_mention) REFERENCES dim_mention(id_mention)
);