CREATE TABLE dim_etablissement (
id_eta number(20) not null,
id_ville number(20),
nombre_public number(20),
nombre_prive number(20),
serie_name varchar2(200),
region_name varchar2(200),
PRIMARY KEY (id_eta)
);