import matplotlib.pyplot as plt
import cx_Oracle
import numpy as np

cnn = cx_Oracle.connect('data/123@localhost/orcl')

cur1 = cnn.cursor()
cur2 = cnn.cursor()

cur1.execute('select * from dim_date')

date = []
for line in cur1:
    date.append(str(line[1])+" _ "+str(line[2]))

cur1.close()



cur2.execute('select p1.classe_name,  p1.nombre_accepte - p2.nombre_accepte from dim_population p1 ,dim_population p2 where p2.id_pob = p1.id_pob+12')

mention_name = []
nombre_mention = []
for line in cur2:
    mention_name.append(line[0])
    nombre_mention.append(line[1])
cur2.close()

tab = []
tab1 = []
k = 0
for i in nombre_mention:
    k = k+1
    if k <= 5:
        tab1.append(i)
    if k>5:
        tab.append(tab1);
        k=0
        tab1 = []


# # the label locations
x = np.arange(len(date)) 

fig = plt.figure()

tt = []
k=0
for t in tab:
    tt.append(0)
    
ax = fig.add_axes([0,0,1,1])
K = [0,1,2,3,4]

for t in tab:
    for i,k in zip(t,K):
        if k==0:
            ax.bar(x + 0.00, i, color = 'b', width = 0.1)
        if k ==1:
            ax.bar(x + 0.25, i, color = 'g', width = 0.1)
        if k==2:
            ax.bar(x + 0.50,i, color = 'r', width = 0.1)
        if k==3:
            ax.bar(x + 0.75, i, color = 'y', width = 0.1)
        if k==4:
            ax.bar(x + 1, i, color = 'black', width = 0.1)
            
ax.legend(labels=set(mention_name))
ax.set_xticklabels(date)

ax.set_title('perfermance des nombres acceptation')
ax.set_xlabel('années')
ax.set_ylabel('nombre_accepte')