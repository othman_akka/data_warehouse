create or replace directory mention_inscrit1
as 'C:\Users\othma\Desktop\datawarehouse\scripts\\';

create or replace directory mention_inscrit2
as 'C:\Users\othma\Desktop\datawarehouse\scripts\\';

create table mention_inscrit(
id_inscrit number(20) ,
id_classe number(20),
id_mention number(20),
nombre number(20),
date_e varchar2(20)
)
organization external
(type oracle_loader
default directory mention_inscrit1
access parameters
(
records delimited by newline
skip 1
characterset UTF8
badfile mention_inscrit2:'import.bad'
logfile mention_inscrit2:'import.log'
fields terminated by ';'
optionally enclosed by '"')
location('mention_inscrit.csv'))
reject limit unlimited;