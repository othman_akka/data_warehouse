create or replace directory classe1
as 'C:\Users\othma\Desktop\datawarehouse\scripts\\';

create or replace directory classe2
as 'C:\Users\othma\Desktop\datawarehouse\scripts\\';

create table classe(
id_classe number(20) ,
classe_name varchar2(20)
)
organization external
(type oracle_loader
default directory classe1
access parameters
(
records delimited by newline
skip 1
characterset UTF8
badfile classe2:'import.bad'
logfile classe2:'import.log'
fields terminated by ';'
optionally enclosed by '"')
location('classes.csv'))
reject limit unlimited;