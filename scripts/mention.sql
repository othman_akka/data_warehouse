create or replace directory mention1
as 'C:\Users\othma\Desktop\datawarehouse\scripts\\';

create or replace directory mention2
as 'C:\Users\othma\Desktop\datawarehouse\scripts\\';

create table mention(
id_mention number(20) ,
mention_name varchar2(20)
)
organization external
(type oracle_loader
default directory mention1
access parameters
(
records delimited by newline
skip 1
characterset UTF8
badfile mention2:'import.bad'
logfile mention2:'import.log'
fields terminated by ';'
optionally enclosed by '"')
location('mention.csv'))
reject limit unlimited;