create or replace directory region1
as 'C:\Users\othma\Desktop\datawarehouse\scripts\\';

create or replace directory region2
as 'C:\Users\othma\Desktop\datawarehouse\scripts\\';

create table region(
id_region number(20) ,
region_name varchar2(20)
)
organization external
(type oracle_loader
default directory mention1
access parameters
(
records delimited by newline
skip 1
characterset UTF8
badfile region2:'import.bad'
logfile region2:'import.log'
fields terminated by ';'
optionally enclosed by '"')
location('region.csv'))
reject limit unlimited;