create table supporter(
id_supp number(20) not null,
id_etablissement  number(20),
id_serie number(20),
PRIMARY KEY (id_supp),
FOREIGN KEY(id_etablissement) REFERENCES etablissement(id_etablissement),
FOREIGN KEY(id_serie) REFERENCES bac(id_serie)
);