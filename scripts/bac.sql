create or replace directory bac1
as 'C:\Users\othma\Desktop\datawarehouse\scripts\\';

create or replace directory bac2
as 'C:\Users\othma\Desktop\datawarehouse\scripts\\';

create table bac(
id_serie number(20) ,
serie_name varchar2(200)
)
organization external
(type oracle_loader
default directory bac1
access parameters
(
records delimited by newline
skip 1
characterset UTF8
badfile bac2:'import.bad'
logfile bac2:'import.log'
fields terminated by ';'
optionally enclosed by '"')
location('bac.csv'))
reject limit unlimited;