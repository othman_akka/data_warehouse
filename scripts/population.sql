create or replace directory population1
as 'C:\Users\othma\Desktop\datawarehouse\scripts\\';

create or replace directory population2
as 'C:\Users\othma\Desktop\datawarehouse\scripts\\';

create table population(
id_population number(20) ,
nombre_demande number(20),
nombre_accepte number(20),
id_classe number(20),
id_serie number(20),
date_p varchar2(20)
)
organization external
(type oracle_loader
default directory population1
access parameters
(
records delimited by newline
skip 1
characterset UTF8
badfile population2:'import.bad'
logfile population2:'import.log'
fields terminated by ';'
optionally enclosed by '"')
location('population.csv'))
reject limit unlimited;