create or replace directory etablissement1
as 'C:\Users\othma\Desktop\datawarehouse\scripts\\';

create or replace directory etablissement2
as 'C:\Users\othma\Desktop\datawarehouse\scripts\\';

create table etablissement(
id_etablissement number(20),
id_ville number(20) ,
numbre_public varchar2(20),
numbre_prive varchar2(20),
ville varchar2(20),
id_region number(20),
date_e varchar2(20)
)
organization external
(type oracle_loader
default directory etablissement1
access parameters
(
records delimited by newline
skip 1
characterset UTF8
badfile etablissement2:'import.bad'
logfile etablissement2:'import.log'
fields terminated by ';'
optionally enclosed by '"')
location('etablissement.csv'))
reject limit unlimited;